// MathLibrary.h - Contains declarations of math functions
#include <list>
#include "CommonHelpers.h"
#pragma once
#include <opencv2/opencv.hpp>
#ifdef BARCODEDETECTOR_EXPORTS
#define BARCODEDETECTOR_API __declspec(dllexport)
#else
#define BARCODEDETECTOR_API __declspec(dllimport)
#endif

// -------------------------------------------------------
// ------------ EXPOSED FUNCTIONS TO OUTSIDE -------------
// -------------------------------------------------------
extern "C" {
    using namespace cv;
    using namespace std;
    namespace bruface {
		BARCODEDETECTOR_API void binarizeImg(Mat inImg, Mat &outStep, int thr_def, int block_size);
    	BARCODEDETECTOR_API bool getQRMarksPoints(Mat inImg, vector<Point2i> &qrPoints, vector<vector<int>> &qrSizes);
		BARCODEDETECTOR_API void orderQrPoints(vector<Point2i>qrPos,  vector<int> &orderIndex);
		BARCODEDETECTOR_API bool getAlignPoints(Mat inImg, vector<Point2i>qrPos, vector<vector<int>> qrSizes, vector<int> orderIndex, Point2i &qrAlign);
		BARCODEDETECTOR_API void getQrImageFromFindersAndAlign(Mat &inImg, Mat &qrImg, vector<Point2i>qrPos, vector<vector<int>> qrSizes, vector<int> orderIndex, Point2i qrAlign, bool isVersionOne);		
    }
}


