// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"
#include <list>
#include "CommonHelpers.h"
// ----------------------------------------------------------------------------
// --------------------- GET PIXEL VALUE --------------------------------------
// --- get the pixel value of a given image at a given coordinates. -----------
// ----------------------------------------------------------------------------
bool getPixelValue(cv::Mat inImg, cv::Point2i point, uchar &value) {
	if (point.y < 0 || point.y >= inImg.rows) {
		value = 255;
		return false;
	}

	if (point.x < 0 || point.x >= inImg.cols) {
		value = 255;
		return false;
	}
	value = inImg.at<uchar>(point.y, point.x);
	return true;
}

// ----------------------------------------------------------------------------
// --------------------- SHIFT REGISTER TO RIGHT ------------------------------
// --- Used when computing the sizes of the QR finders  -----------------------
// ----------------------------------------------------------------------------
void shiftRight(int *p, int numElem) {
	for (int i = numElem - 1; i > 0; i--) {
		*(p + i) = *(p + i - 1);
	}
}

// ----------------------------------------------------------------------------
// --------------------- IS WHITE LINE  ---------------------------------------
// --- return true if the line between two points is white --------------------
// ----------------------------------------------------------------------------
bool isWhiteLine(Mat inImg, Point2i p1, Point2i p2) {
	Point2f unitDir = p2 - p1;
	unitDir = unitDir / norm(unitDir);
	bool isWhite = true;
	uchar pixel;
	Point2i movingPoint = p1;
	int nIncrement = 0;
	while (movingPoint != p2) {
		updatePointInDir(p1, unitDir, nIncrement, movingPoint);
		if (getPixelValue(inImg, movingPoint, pixel)) {
			if (pixel < THR) {
				isWhite = false;
				break;
			}
		}
		nIncrement++;
	}

	return isWhite;
}
// ----------------------------------------------------------------------------
// --------------------- UPDATE POINT IN SPECIFIC DIRECTION -------------------
// --- compute the next point to analize in an specific direction -------------
// ----------------------------------------------------------------------------

void updatePointInDir(Point2i inPoint, Point2f unitDir, int nIncrement, Point2i &outPoint) {
	outPoint = inPoint + Point2i(unitDir*nIncrement);
}
// ----------------------------------------------------------------------------
// --------------------- GET FINDER SIZE IN A SPECIFIC DIRECTION --------------
// --- compute the best estimation of the size in a specific direction --------
// ----------------------------------------------------------------------------
float getModuleSizeInDir(Point2f unitDir, int sizesA[], int sizesB[]) {

	float finderSize;
	float corrH, corrV, corrAsc, corrDsc;
	corrH = abs(unitDir.x);
	corrV = abs(unitDir.y);
	corrAsc = abs(unitDir.x + unitDir.y) / norm(Point2i(1, 1));
	corrDsc = abs(unitDir.x - unitDir.y) / norm(Point2i(1, 1));

	if (corrH >= corrV && corrH >= corrAsc && corrH >= corrDsc) {
		finderSize = (sizesA[DIR_HRZ] + sizesB[DIR_HRZ])*corrH / 2;
	}
	else if (corrV >= corrH && corrV >= corrAsc && corrV >= corrDsc) {
		finderSize = (sizesA[DIR_VRT] + sizesB[DIR_VRT])*corrV / 2;
	}
	else if (corrAsc >= corrH && corrAsc >= corrV && corrAsc >= corrDsc) {
		finderSize = (sizesA[DIR_ASC] + sizesB[DIR_ASC])*corrAsc / 2;
	}
	else {
		finderSize = (sizesA[DIR_DSC] + sizesB[DIR_DSC])*corrDsc / 2;
	}
	return finderSize / 7.0;
}

// ----------------------------------------------------------------------------
// --------------------- GUESS VERSION OF QR CODE -----------------------------
// --- return an estimation of the version of QR code base on 2 qrFinders -----
// ----------------------------------------------------------------------------
int gessVersion(QRMark A, QRMark B, int &nModules) {
	float dist = norm(A.center - B.center);
	Point2f unitDist = A.center - B.center;
	unitDist = unitDist / norm(unitDist);
	// choose direction:
	float finderSize = getModuleSizeInDir(unitDist, A.sizes, B.sizes);
	float nMod = dist / finderSize;
	/*
	version = (modules - 21) / 4 + 1
	modules = (version - 1)*4 +21
	1: 21 mod.	11: 61 mod.
	2: 25 mod.	12: 65 mod.
	3: 29 mod.	13: 69 mod.
	4: 33 mod.	14: 73 mod.
	5: 37 mod.	15: 77 mod.
	6: 41 mod.	16: 81 mod.
	7: 45 mod.	17: 85 mod.
	8: 49 mod.	18: 89 mod.
	9: 53 mod.	19: 93 mod.
	10:57 mod.	20: 97 mod...... until version 40
	*/

	float aprox = (nMod - 21 + 7) / 4 + 1;
	int version = (aprox - (int)aprox) >= 0.5 ? (int)ceil(aprox) : (int)floor(aprox);
	nModules = (version - 1) * 4 + 21;
	return version;
}
