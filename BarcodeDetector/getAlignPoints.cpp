// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"

#define NEIGHBOR_SIDE 15 
#define EPSILON_AREA 1

// local helper functions
void getAlignModCoord(int version, Point2i &alignModCoord);

namespace bruface {
	using namespace cv;
	using namespace std;
	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 4: FIND QR-ALIGNMENT ------------------------------------
	// --------------------------------------------------------------------------------------

	bool getAlignPoints(Mat inImg, vector<Point2i>qrPos, vector<vector<int>> qrSizes, vector<int> orderIndex, Point2i &qrAlign){
		QRMark qrUL, qrUR, qrLL;
		Point2i amc; // align Module Coordinate
		cv::Mat mask = cv::Mat::zeros(inImg.rows, inImg.cols, CV_8UC1);
		cv::Mat drawing = cv::Mat::zeros(inImg.rows, inImg.cols, CV_8UC3);
		cv::Mat drawing2 = cv::Mat::zeros(inImg.rows, inImg.cols, CV_8UC3);
		cv::Mat alignNeighborhood;

		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;
		vector<Point> pointsseq;

		// ----------------------- Arranging data in correct format.
		qrUL.center = qrPos.at(orderIndex.at(UL));
		copy(qrSizes.at(orderIndex.at(UL)).begin(), qrSizes.at(orderIndex.at(UL)).end(), qrUL.sizes);
		
		qrUR.center = qrPos.at(orderIndex.at(UR));
		copy(qrSizes.at(orderIndex.at(UR)).begin(), qrSizes.at(orderIndex.at(UR)).end(), qrUR.sizes);

		qrLL.center = qrPos.at(orderIndex.at(LL));
		copy(qrSizes.at(orderIndex.at(LL)).begin(), qrSizes.at(orderIndex.at(LL)).end(), qrLL.sizes);
		
		Point2f unitUlUr = qrUL.center - qrUR.center;
		Point2f unitUlLl = qrUL.center - qrLL.center;
		unitUlUr = unitUlUr / norm(unitUlUr);
		unitUlLl = unitUlLl / norm(unitUlLl);
		// ------------------- Version and aligns coordinates
		int nModules;
		int version = gessVersion(qrUL, qrUR, nModules);
		if (version == 1) {
			return false;
		}

		// ------- GET FIRST ESTIMATION OF ALIGNMENT -------
		getAlignModCoord(version, amc);
		
		// ------------------- Try to find the most outer Align Mark
		float modSizeUlUr = getModuleSizeInDir(unitUlUr, qrUR.sizes, qrUL.sizes);
		float modSizeUlLl = getModuleSizeInDir(unitUlLl, qrUR.sizes, qrLL.sizes);
		
		Point2f nP = Point2f(qrUL.center)							// starting point 
			+ 3 * modSizeUlUr*unitUlUr
			+ 3 * modSizeUlLl*unitUlLl								// correction to the origin.
			- amc.x*modSizeUlUr*unitUlUr							// in UrUl direction
			- amc.y*modSizeUlLl*unitUlLl
			;
		int modSize = (modSizeUlUr + modSizeUlLl) / 2;
	
		// ---------------------------------------------------------------------------------
		//------------ Retrieve the neighbour and its neighborhood ------------------------
		// ---------------------------------------------------------------------------------
		circle(mask, nP, NEIGHBOR_SIDE*modSize, Scalar(255, 255, 255), -1, 8, 0); //-1 means filled
		inImg.copyTo(alignNeighborhood, mask); // copy values of img to dst if mask is > 0.
		
		// find the alignment based on contours: squared contour having only one child of different color = alignment
		findContours(alignNeighborhood, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		Point2i alignCndt;
		bool found = false;
		for (int c = 0; c< contours.size(); c++)
		{
			approxPolyDP(contours[c], pointsseq, arcLength(contours[c], true)*0.05, true);
			if (pointsseq.size() == 4) {     // If the contour is squared. analyzed
				int childIndex = hierarchy[c][2];
				// the child contour is the most inner.
				bool hasCorrectChild = childIndex == -1 ? false : hierarchy[childIndex][2] == -1; 				
				if (hasCorrectChild) {
					Moments mu = moments(contours[c], false);
					Point2i countourCenter = Point2i(mu.m10 / mu.m00, mu.m01 / mu.m00);
					uchar pixelCenter;
					getPixelValue(inImg, countourCenter, pixelCenter);
					if (pixelCenter < 100) {
						if (mu.m00 >(1.5 * modSize * 1.5 * modSize) && mu.m00 <(5 * modSize * 5 * modSize)) {							// the area is close to 3x3 modules
							alignCndt = countourCenter;
							found = true;
							break;
						}
					}					
				}
				
			}
		}
		if (found) {
			qrAlign = alignCndt;
		}
		else {
			qrAlign = Point2f(qrUL.center)								// starting point 
				+ 3 * modSizeUlUr*unitUlUr
				+ 3 * modSizeUlLl*unitUlLl								// correction to the origin.
				- amc.x*modSizeUlUr*unitUlUr							// in UrUl direction
				- amc.y*modSizeUlLl*unitUlLl
				;
		}
		return version == 1;
	}
}

// ------------------------------------------------------------------------------------------------
// ------------------------------ HELPER FUNCTIONS ------------------------------------------------
// ------------------------------------------------------------------------------------------------

void getAlignModCoord(int version, Point2i &alignModCoord) {
	// to do: Generalize this part to retrieve the coordinates of ALL finders. so far only the furthest
	int nModules = (version - 1) * 4 + 21;
	Point2i lastAlign(nModules - 6, nModules - 6);
	alignModCoord = lastAlign;
}