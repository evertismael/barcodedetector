// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"
#define LOW_LEVEL_CNT 2
#define EPS 0.50
#define MAX_NEIGHBOR 128*4

// ---------------------------------------------------------
// ---------------- local helper functions -----------------
// ---------------------------------------------------------

void getCandidates(Mat inImg, vector<QRMark> &candidates);

bool isCandidatePresent(vector<QRMark> &qRMarks, Point2i cndtCenter, int &index);
void validateCandidates(Mat inImg, vector<QRMark> inCndts, int direction, vector<QRMark> &outCndts);
bool checkRelationship(int cnt[5]);
void addCandidate(vector<QRMark> &qRMarks, QRMark cndt);
// ---------------------------------------------------------

namespace bruface {
	using namespace cv;
	using namespace std;
	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 3: FIND QR-FINDERS --------------------------------------
	// ------------------ and order them if they are 3 --------------------------------------
	bool getQRMarksPoints(Mat inImg, vector<Point2i> &qrPoints, vector<vector<int>> &qrSizes) {
		bool result = false;
		uchar lastColorX = 255;	
		vector<QRMark> qrPointsCandidates;
		int version;
				
		// ----------- Find Candidate Points that satisfy the condition in X-axis direction ---------------
		getCandidates(inImg, qrPointsCandidates);
		if (qrPointsCandidates.size() == 0) {
			return false;
		}
		// ---------------- Check the condition in Y-axis direction -------------------------
		validateCandidates(inImg, qrPointsCandidates, DIR_VRT, qrPointsCandidates);
		if (qrPointsCandidates.size() == 0) {
			return false;
		}
		// ----------------- Check the condition in X=Y-axis ----------------------------
		validateCandidates(inImg, qrPointsCandidates, DIR_ASC, qrPointsCandidates);
		if (qrPointsCandidates.size() == 0) {
			return false;
		}
		// ----------------- Check the condition in X=-Y-axis ----------------------------
		validateCandidates(inImg, qrPointsCandidates, DIR_DSC, qrPointsCandidates);
		if (qrPointsCandidates.size() == 0) {
			return false;
		}
		
		//------------ finally copy the points outside ------------------------
		for (int i = 0; i < qrPointsCandidates.size(); i++) {
			qrPoints.push_back(qrPointsCandidates.at(i).center);
			vector<int> sizes;
			sizes.push_back(qrPointsCandidates.at(i).sizes[DIR_HRZ]);
			sizes.push_back(qrPointsCandidates.at(i).sizes[DIR_VRT]);
			sizes.push_back(qrPointsCandidates.at(i).sizes[DIR_ASC]);
			sizes.push_back(qrPointsCandidates.at(i).sizes[DIR_DSC]);
			qrSizes.push_back(sizes);
		}
		result = true;		
		return result;
	}
}

// ------------------------------------------------------------------------------------------------
// ------------------------------ HELPER FUNCTIONS ------------------------------------------------
// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------
// Sweep the whole image in horizontal direction to find the relationship 1:1:3:1:1 between the 
// change of colors: black,white,black,white,black
// ------------------------------------------------------------------------------------------------
void getCandidates(Mat inImg, vector<QRMark> &candidates) {
	uchar lastColorX;
	uchar valx;

	for (int y = 0; y < inImg.rows; y++) { // y for cols
		int cntIncrements[] = { 1,0,0,0,0,0 }; // 1:1:3:1:1
		lastColorX = 255;
		for (int x = 0; x < inImg.cols; x++) { // x for rows
			getPixelValue(inImg, Point2i(x, y), valx);			
			if (lastColorX == valx) {
				cntIncrements[0]++;
			}
			else {
				shiftRight(cntIncrements, 6);
				if (lastColorX < valx) { // only check if the transition is from black to white
					if (checkRelationship(cntIncrements)) { // X pattern 1:1:3:1:1
						int nx = x - cntIncrements[2] - cntIncrements[1] - (cntIncrements[3] / 2);
						QRMark cndt;
						cndt.center = Point2i(nx, y);
						cndt.sizes[DIR_HRZ] = cntIncrements[1] + cntIncrements[2] + cntIncrements[3] + cntIncrements[4] + cntIncrements[5];
						candidates.push_back(cndt);
					}
				}
				cntIncrements[0] = 1;
			}
			lastColorX = valx;
		}
	}
}

// ------------------------------------------------------------------------------------------------
// Check the relationship in a given direction, relationship 1:1:3:1:1 between the 
// change of colors: black,white,black,white,black
// ------------------------------------------------------------------------------------------------
void validateCandidates(Mat inImg, vector<QRMark> inCndts, int direction, vector<QRMark> &outCndts) {
	vector<QRMark> tempCndts;
	Point2f unitDir;
	int changesUp = 0, changesDown = 0;
	int nIncrementUp = 0, nIncrementDown = 0;
	uchar lastColorUp, lastColorDown;
	uchar color;
	Point2i center, movingPoint;

	//Mat dM;
	//inImg.copyTo(dM);

	// select direction:
	if (direction == DIR_VRT) {
		unitDir = Point2i(0, 1);
	}else if(direction == DIR_ASC) {
		unitDir = Point2f(1.0, 1.0) / norm(Point2i(1, 1));
	}else if (direction == DIR_DSC) {
		unitDir = Point2f(1.0, -1.0) / norm(Point2i(1, 1));
	}else {
		return;
	}

	int index;
	
	for (int i = 0; i < inCndts.size(); i++) {	
		
		//inImg.copyTo(dM);

		if (isCandidatePresent(tempCndts, inCndts.at(i).center, index)) {
			inCndts.at(index).cntFreq++;
		}
		else {
			int cntIncrements[] = { 0,0,0,0,0,0 }; // 1:1:3:1:1
			center = inCndts.at(i).center;
			nIncrementUp = 0, nIncrementDown = 0;
			changesUp = 0, changesDown = 0;
			getPixelValue(inImg, center, lastColorUp);
			lastColorDown = lastColorUp;

			
			int Iteration = 0;
			while ((changesUp + changesDown < 6) && (nIncrementUp + nIncrementDown) < MAX_ITERATIONS && Iteration < MAX_NEIGHBOR) {
				// --- up
				if (changesUp < 3) {
					nIncrementUp++;
					updatePointInDir(center, unitDir, nIncrementUp, movingPoint);
					getPixelValue(inImg, movingPoint, color);
					if (lastColorUp == color) {
						cntIncrements[3 - changesUp]++;
					}
					else {
						changesUp++;
						lastColorUp = color;
					}
					//circle(dM, movingPoint, 3, (0, 0, 150), -1);
				}

				// --- down
				if (changesDown < 3) {
					nIncrementDown++;
					updatePointInDir(center, -unitDir, nIncrementDown, movingPoint);
					getPixelValue(inImg, movingPoint, color);
					if (lastColorDown == color) {
						cntIncrements[3 + changesDown]++;
					}
					else {
						changesDown++;
						lastColorDown = color;
					}
					//circle(dM, movingPoint, 3, (0, 0, 150), -1);
				}

				Iteration++;
				//imshow("aaa", dM);
				//waitKey();
			}

			if (changesUp + changesDown == 6) { // if there is changes b-w-b-w
				bool valid = checkRelationship(cntIncrements);
				if (valid) {
					updatePointInDir(center, unitDir, (nIncrementUp - nIncrementDown) / 2, movingPoint);
					QRMark temp = inCndts.at(i);
					temp.center = movingPoint;
					temp.sizes[direction] = nIncrementUp + nIncrementDown;
					addCandidate(tempCndts, temp);
				}
			}
		}
	}	
	outCndts = tempCndts;
}

// ------------------------------------------------------------------------------------------------
// return true if the elements satisfy the following relationship 1:1:3:1:1
// ------------------------------------------------------------------------------------------------
bool checkRelationship(int cnt[5]) {
	// check relation 1:1:3:1:1
	bool res = false;

	res = cnt[1] > LOW_LEVEL_CNT && cnt[2] > LOW_LEVEL_CNT && cnt[3] > LOW_LEVEL_CNT
		&& cnt[4] > LOW_LEVEL_CNT && cnt[5] > LOW_LEVEL_CNT;

	if (!res) return res;
	float rel1 = (3.0 * cnt[1]) / cnt[3];
	float rel2 = (3.0 * cnt[2]) / cnt[3];
	float rel3 = (3.0 * cnt[4]) / cnt[3];
	float rel4 = (3.0 * cnt[5]) / cnt[3];

	res = res && abs(rel1 - 1.0) < EPS;
	res = res && abs(rel2 - 1.0) < EPS;
	res = res && abs(rel3 - 1.0) < EPS;
	res = res && abs(rel4 - 1.0) < EPS;
	return res;
}

// ------------------------------------------------------------------------------------------------
// Add a point candidate that satisfy the relationship, if it is already included it increase the
// frequency of ocurrence, and take the average of the size in the analized direction. It also tries
// to group all the points that are close such that only remains the representative ones.
// ------------------------------------------------------------------------------------------------
void addCandidate(vector<QRMark> &qRMarks, QRMark cndt) {
	// ---------- not proximus to any qrMark already present --------
	int i;
	if (isCandidatePresent(qRMarks, cndt.center, i)) {
		qRMarks.at(i).cntFreq++;
		qRMarks.at(i).center = (qRMarks.at(i).center + cndt.center) / 2;
		for (int j = 0; j < 4; j++) {
			if (qRMarks.at(i).sizes[j] == 0) {
				qRMarks.at(i).sizes[j] = cndt.sizes[j];
			} else {
				qRMarks.at(i).sizes[j] = cndt.sizes[j] == 0 ? qRMarks.at(i).sizes[j] : (qRMarks.at(i).sizes[j] + cndt.sizes[j]) / 2;
			}
		}
	}
	else {
		cndt.cntFreq++;
		qRMarks.push_back(cndt);
	}
}

// ------------------------------------------------------------------------------------------------
// true if the candidate point is already present in the array of candidates.
// ------------------------------------------------------------------------------------------------
bool isCandidatePresent(vector<QRMark> &qRMarks, Point2i cndtCenter, int &index) {
	// ---------- not proximus to any qrMark already present --------
	bool result = false;
	for (int i = 0; i < qRMarks.size(); i++) {
		if (norm(qRMarks.at(i).center - cndtCenter)<qRMarks.at(i).sizes[DIR_HRZ]) {
			result = true;
			index = i;
			break;
		}
	}
	return result;
}
