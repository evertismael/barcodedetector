// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"

namespace bruface {
    using namespace cv;
    using namespace std;

	// --------------------------------------------------------------------------------------
	// ---------------- STAGE 2: BINARIZATION - LOCAL THRESHOLDING --------------------------
	// --------------------------------------------------------------------------------------
	
	void binarizeImg(Mat inImg, Mat &outStep, int thr_def, int block_size) {
		Mat grayImg(inImg.size(), CV_MAKETYPE(inImg.depth(), 1));
		Mat binaryImage(inImg.size(), CV_MAKETYPE(inImg.depth(), 1));

		cvtColor(inImg, grayImg, CV_RGB2GRAY);			// Converting input image to GrayScale
		adaptiveThreshold(grayImg, binaryImage, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, block_size, 20);
		binaryImage.copyTo(outStep);
	}
}