#pragma once
#include <opencv2/opencv.hpp>
// -------------------------------------------------------
// ------------ CONSTANTS --------------------------------
// -------------------------------------------------------
#define UL 0
#define LL 1
#define UR 2

#define DIR_HRZ 0
#define DIR_VRT 1
#define DIR_ASC 2
#define DIR_DSC 3

#define MAX_ITERATIONS 1000

#define THR 100			// pixel value between white and black

struct QRMark {
	cv::Point2i center;
	int sizes[4] = { 0,0,0,0 }; // H,V,A,D
	int cntFreq = 0;
};

// -------------------------------------------------------
// ------------ COMMON FUNCTION HELPERS ------------------
// -------------------------------------------------------
bool getPixelValue(cv::Mat inImg, cv::Point2i point, uchar &value);
void shiftRight(int *p, int numElem);
bool isWhiteLine(cv::Mat inImg, cv::Point2i p1, cv::Point2i p2);
void updatePointInDir(cv::Point2i inPoint, cv::Point2f unitDir, int nIncrement, cv::Point2i &outPoint);
float getModuleSizeInDir(cv::Point2f unitDir, int sizesA[], int sizesB[]);
int gessVersion(QRMark A, QRMark B, int &nModules);
