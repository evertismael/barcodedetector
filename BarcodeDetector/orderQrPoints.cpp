// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"

namespace bruface {
	using namespace cv;
	using namespace std;
	//--------------------------------------------------------------------------
	// ---- return an array with the indexes of qrFinders in the following order
	// orderIndex = [qr1_ul, qr1_ll, qr1_ur];
	// only define ul,ll,ur but the points must be filtered before;
	void orderQrPoints(vector<Point2i>qrPos,vector<int>&orderIndex) {
		if (qrPos.size() % 3 != 0) {
			cout << "OrDerQrPoints: taking only first 3 points" << endl;
		}
		int upperLeft = -1, lowerLeft = -1, upperRight = -1, aux;

		Point2i AB = qrPos.at(0) - qrPos.at(1);
		Point2i AC = qrPos.at(0) - qrPos.at(2);
		Point2i BC = qrPos.at(1) - qrPos.at(2);

		// ------ choose UL
		if (norm(AB) > norm(BC) && norm(AB) > norm(AC)) { // c in the middle
			upperLeft = 2;
			lowerLeft = 0; upperRight = 1;
		}
		else if (norm(AC) > norm(AB) && norm(AC) > norm(BC)) { // b in the middle
			upperLeft = 1;
			lowerLeft = 0; upperRight = 2;
		}
		else { // a in the middle
			upperLeft = 0;
			lowerLeft = 1; upperRight = 2;
		}

		// ------ choose LL and UR
		Point2i dir = qrPos.at(upperRight) - qrPos.at(lowerLeft);
		Point2f unitDir = Point2f(dir) / norm(dir);
		Point2i p = qrPos.at(upperLeft) - qrPos.at(lowerLeft);
		Point2i d = Point2i(Point2f(p) - (p.x*unitDir.x + p.y*unitDir.y)*unitDir);

		bool sameX = (dir.x >= 0 && d.x >= 0) || (dir.x < 0 && d.x < 0);
		bool sameY = (dir.y >= 0 && d.y >= 0) || (dir.y < 0 && d.y < 0);
		bool pstvSlope = !((dir.x >= 0 && dir.y >= 0) || (dir.x < 0 && dir.y < 0)); // negated cause y axis is positive downwards in opencv
		
		if ( (pstvSlope && sameX && !sameY) || (!pstvSlope && !sameX && sameY)){
			aux = lowerLeft;
			lowerLeft = upperRight;
			upperRight = aux;
		}

		// saving the indexes.
		orderIndex.push_back(upperLeft);
		orderIndex.push_back(lowerLeft);
		orderIndex.push_back(upperRight);
	}

}