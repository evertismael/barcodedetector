// MathLibrary.cpp  Defines the exported functions for the DLL.
#include "stdafx.h"
#include <iostream>
#include "BarcodeDetector.h"
#define MODULE_BORDER 2
#define EPSILON_AREA 1

// local helper functions
namespace bruface {
	using namespace cv;
	using namespace std;
	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 5: EXTRACT QR-CODE --------------------------------------
	// --------------------------------------------------------------------------------------

	void getQrImageFromFindersAndAlign(Mat &inImg, Mat &qrImg, vector<Point2i>qrPos, vector<vector<int>>qrSizes, vector<int> orderIndex, Point2i qrAlign, bool isVersionOne){

		Point2i Ul, Ur, Ll, algn;
		float modSizeUr_UlUr, modSizeUr_AlgnUr;
		float modSizeUl_UlLl, modSizeUl_UlUr;
		float modSizeLl_UlLl, modSizeLl_AlgnLl;
		float modSizeAlgn_AlgnLl, modSizeAlgn_AlgnUr;

		Point2i cornerUl, cornerUr, cornerLl,cornerLr;
		int sizesUl[4], sizesUr[4], sizesLl[4];
		Point2f unitUlUr, unitUlLl, unitAlgnUr, unitAlgnLl;
		
		Ur = qrPos.at(orderIndex.at(UR));
		Ul = qrPos.at(orderIndex.at(UL));
		Ll = qrPos.at(orderIndex.at(LL));
		
		copy(qrSizes.at(orderIndex.at(UR)).begin(), qrSizes.at(orderIndex.at(UR)).end(), sizesUr);
		copy(qrSizes.at(orderIndex.at(UL)).begin(), qrSizes.at(orderIndex.at(UL)).end(), sizesUl);
		copy(qrSizes.at(orderIndex.at(LL)).begin(), qrSizes.at(orderIndex.at(LL)).end(), sizesLl);

		unitUlUr = (Ul - Ur);
		unitUlUr = unitUlUr / norm(Ul - Ur);
		unitUlLl = (Ul - Ll);
		unitUlLl  = unitUlLl / norm(Ul - Ll);
		
		// computing the directional vectors with the finder
		if (!isVersionOne) {
			algn = qrAlign;

			Point2i corUr, corLl;
			float sizesUlUr = getModuleSizeInDir(unitUlUr, sizesUl, sizesUr);
			float sizesUlLl = getModuleSizeInDir(unitUlLl, sizesUl, sizesLl);


			updatePointInDir(Ur, unitUlUr, sizesUlUr * 3, corUr);
			updatePointInDir(corUr, -unitUlLl, sizesUlLl * 3, corUr);

			updatePointInDir(Ll, -unitUlUr, sizesUlUr * 3, corLl);
			updatePointInDir(corLl, unitUlLl, sizesUlLl * 3, corLl);

			unitAlgnUr = Point2f(algn - corUr) / norm(algn - corUr);
			unitAlgnLl = Point2f(algn - corLl) / norm(algn - corLl);

			// sizes in specific directions.
			modSizeUl_UlLl = getModuleSizeInDir(unitUlLl, sizesUl, sizesUl); //UL
			modSizeUl_UlUr = getModuleSizeInDir(unitUlUr, sizesUl, sizesUl);

			modSizeUr_UlUr = getModuleSizeInDir(unitUlUr, sizesUr, sizesUr);
			modSizeUr_AlgnUr = getModuleSizeInDir(unitAlgnUr, sizesUr, sizesUr);

			modSizeLl_UlLl = getModuleSizeInDir(unitUlLl, sizesLl, sizesLl);
			modSizeLl_AlgnLl = getModuleSizeInDir(unitAlgnLl, sizesLl, sizesLl);
			
			// choose the best sizes for the Align mark (choose the one with lowest paralelism)
			// compare the degree of paralelism of unitAlgnLl <->unitUlUr and unitAlgnUr <-> unitUlLl, 

			float innerAlgnLl_UlUr = unitAlgnLl.x * unitUlUr.x + unitAlgnLl.y*unitUlUr.y;
			float innerAlgnUr_UlLl = unitAlgnUr.x * unitUlLl.x + unitAlgnUr.y*unitUlLl.y;

			if (abs(innerAlgnLl_UlUr) > abs(innerAlgnUr_UlLl)) { // choose ULLl
				modSizeAlgn_AlgnLl = getModuleSizeInDir(unitAlgnLl, sizesLl, sizesLl);
				modSizeAlgn_AlgnUr = getModuleSizeInDir(unitAlgnUr, sizesLl, sizesLl);
			}
			else {
				modSizeAlgn_AlgnLl = getModuleSizeInDir(unitAlgnLl, sizesUr, sizesUr);
				modSizeAlgn_AlgnUr = getModuleSizeInDir(unitAlgnUr, sizesUr, sizesUr);
			}

			// ----------- start computing the corners
			updatePointInDir(Ul, unitUlUr, modSizeUl_UlUr * (3 + MODULE_BORDER), cornerUl);			// cornerUl
			updatePointInDir(cornerUl, unitUlLl, modSizeUl_UlLl * (3 + MODULE_BORDER), cornerUl);
			
			updatePointInDir(Ll, -unitUlLl, modSizeLl_UlLl * (3 + MODULE_BORDER), cornerLl);			// cornerLl
			updatePointInDir(cornerLl, -unitAlgnLl, modSizeLl_AlgnLl * (3 + MODULE_BORDER), cornerLl);
			
			updatePointInDir(Ur, -unitUlUr, modSizeUr_UlUr * (3 + MODULE_BORDER), cornerUr);			// cornerUr
			updatePointInDir(cornerUr, -unitAlgnUr, modSizeUr_AlgnUr * (3 + MODULE_BORDER), cornerUr);
		

			updatePointInDir(algn, unitAlgnUr, modSizeAlgn_AlgnUr * (6 + MODULE_BORDER), cornerLr);			// cornerLr
			updatePointInDir(cornerLr, unitAlgnLl, modSizeAlgn_AlgnLl * (6 + MODULE_BORDER), cornerLr);

		}
		
		// apply the transformation:
		vector<Point2f> source, dst;
		Mat warp_matrix; // qr_exact.

		// the dst vector points must be in the same order.
		dst.push_back(Point2f(0, 0));
		dst.push_back(Point2f(qrImg.cols, 0));
		dst.push_back(Point2f(0, qrImg.rows));
		dst.push_back(Point2f(qrImg.cols, qrImg.rows));


		source.push_back(cornerUl);
		source.push_back(cornerUr);
		source.push_back(cornerLl);
		source.push_back(cornerLr);

		warp_matrix = getPerspectiveTransform(source, dst);
		warpPerspective(inImg, qrImg, warp_matrix, Size(qrImg.cols, qrImg.rows));

	}
}