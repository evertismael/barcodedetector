# README #

This repository summarizes all the steps followed to complete the project "2D barcode scanning and decoding" required to approb the course "Project of Electronics and Telecommunication". Starting date: October 3rd, 2017. Supervisor of the project: Prof. Adrian Munteanu.
.
### What is this repository for? ###

* Quick summary
The scope of this project is to devise an image processing algorithm for 2D barcode scanning for industrial environments for large distances and difficult operational conditions. Requirements: knowledge in C/C++ programming, signal processing. This topic can be continued later on with a MSc thesis on embedded vision.
* Version
0.1
### Personal Data ###

Name: Evert I. Pocoma Copa.
email: epocomac@vub.be