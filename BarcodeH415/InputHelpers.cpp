/** Author: Evert Ismael Pocoma Copa
* Date : 11 - 11 - 2017
* InputHelpers : Helper functions, constants, etc., involved in the input stage 1.
**/
#include "stdafx.h"
#include "InputHelpers.h"
#include <opencv2/opencv.hpp>
#include <filesystem>

using namespace std;
using namespace std::experimental::filesystem::v1;

void InputHelpers::ParseInputParams(int argc, char* argv[], InputHelpers::SourceTypes& sourceType, std::vector<std::string> &inputArgument) {
	if (argc == 1) {
		cout << "WARNING: A default camera mode will be used as an input" << endl;
		//--
		sourceType = InputHelpers::DIRECTORY;
		std::string main_dir;
		main_dir = "qr_datasets/";
		std::vector<std::string> r;
		for (auto& p : recursive_directory_iterator(main_dir))
			if (p.status().type() == file_type::directory)
				inputArgument.push_back(p.path().string());
		//--
		return;
	}

	// capture from CAMERA
	if (strcmp(argv[1], "camera") == 0 ) {
		sourceType = InputHelpers::LIVE_VIDEO;
		inputArgument.push_back("0"); // default first available camera .
	}
	else if (strcmp(argv[1], "image") == 0) {
		sourceType = InputHelpers::SINGLE_IMAGE;
		inputArgument.push_back("./otherTestImages/test12.jpg");
	}
	else if (strcmp(argv[1], "directory") == 0) {
		sourceType = InputHelpers::DIRECTORY;
		std::string main_dir;
		main_dir = "qr_datasets/";
		std::vector<std::string> r;
		for (auto& p : recursive_directory_iterator(main_dir))
			if (p.status().type() == file_type::directory)
				inputArgument.push_back(p.path().string());

	}
	else {
		throw exception("Option not implemented.\n");
	}
}



void InputHelpers::GetImages(string directory, vector<cv::String> &images) {
	vector<string> formats = { "/*.jpg" ,"/*.JPG", "/*.jpeg", "/*.JPEG" };//
	vector<cv::String> images_aux;

	for (int i = 0; i < formats.size(); i++) {
		images_aux.clear();
		glob(directory + formats.at(i), images_aux, false);
		if (images_aux.size() > 0) {
			images.insert(images.end(), images_aux.begin(), images_aux.end());
		}
	}
}