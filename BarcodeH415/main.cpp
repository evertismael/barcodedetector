// BarcodeH415.cpp : Defines the entry point for the console application.
/** Author: Evert Ismael Pocoma Copa
* Date : 11 - 11 - 2017
* Defines the global wrapper of the project, it is composed of a pipeline of 5 stages.
*	stage 1: 'INPUT' -- inplemented in this project
*			It composes of the logic of data acquiquition. (video, directory, single image)
*   stage 2,3,4,5,6 are called in the file 'FunctionDecodeProcess.cpp', but implemented in the library: BarcodeDetector
**/
#include "stdafx.h"
using namespace std;
using namespace zxing;
using namespace zxing::qrcode;
using namespace cv;
#define DISP_ALL_CONTINUOUS 3
#define DISP_ALL_PAUSED 2
#define DISP_CONSOLE 1

int main(int argc, char* argv[])
{
	// ------------- Logger File -------------------
	// -- This creates the log file for reports ----
	LoggerPlog().LoggerInit(); // Only call it here.
	LoggerPlog().LogError("Initiating Testing");

	// -------------- Defining variables -------------------
	InputHelpers::SourceTypes sourceType;			// if input is camera, image, or directory
	vector<string> inputArgument;					// imageName,cameraID,pathDirectory
	
	
	Mat inImg, inImgBin;	// openCV matrices for partial results
    Mat qrImg = Mat::zeros(350, 350, CV_8UC1);	
	vector<Point2i> qrPos;			// qr-Finder coordinates
	Point2i qrAlign;				// qr-Align coordinate
	vector<int> orderIndex;			// order UL,LL,UR
	
	string qrDecodedMessage;	
	int stage = 0;
   	
	try
	{
		// --------------------------------------------------------------------------------------
		// --------------------------------- STAGE 1: INPUT -------------------------------------
		// --------------------------------------------------------------------------------------

		InputHelpers::ParseInputParams(argc, argv, sourceType, inputArgument); // validate input arguments
		
		//------------------------------- Work  with only one image -----------------------------
		if (sourceType == InputHelpers::SINGLE_IMAGE) {
			inImg = imread(inputArgument.at(0)); // image to work
			if (inImg.empty()) {
				throw exception("ERR: Unable to find image.\n");
			}	
			// --------------------------------------------------------------------------------------
			// --------------------- Executing STAGE 2,3,4,5,6 ---------------------------------------
			// --------------------------------------------------------------------------------------
			stage = DecodeProcess(inImg, inImgBin, qrImg,qrPos,orderIndex, qrAlign, qrDecodedMessage);
			DisplaySteps(inImg, inImgBin,qrImg,qrPos,orderIndex,qrAlign, qrDecodedMessage, stage, DISP_ALL_CONTINUOUS);
		}

		//------------------------------- Work with images in directory  ------------------------
		else if (sourceType == InputHelpers::DIRECTORY) {
			int gtotal = 0, gQrFound = 0, gQrDecoded = 0;
			for (int dir_i = 0; dir_i < inputArgument.size(); dir_i++) {
				vector<cv::String> images;
				InputHelpers::GetImages(inputArgument.at(dir_i), images);

				int total = (int)images.size(), qrFound = 0, qrDecoded = 0;
				for (int i = 0; i < images.size(); i++) {	
					cout << images[i] << endl;
					inImg = imread((string)images[i]); // image to work
					qrPos.clear();
					orderIndex.clear();
					if (inImg.empty()) {
						throw exception("ERR: Unable to find image.\n");
					}
					// --------------------------------------------------------------------------------------
					// --------------------- Executing STAGE 2,3,4,5,6 ---------------------------------------
					// --------------------------------------------------------------------------------------
					stage = DecodeProcess(inImg, inImgBin, qrImg, qrPos, orderIndex, qrAlign, qrDecodedMessage);
					DisplaySteps(inImg, inImgBin, qrImg, qrPos, orderIndex, qrAlign, qrDecodedMessage, stage, DISP_ALL_PAUSED);

					// further counting for report
					qrFound = stage > 3 ? qrFound+1: qrFound;
					qrDecoded = stage == 5 ? qrDecoded + 1 : qrDecoded;
				}
				gtotal += total;
				gQrFound += qrFound;
				gQrDecoded += qrDecoded;
				// log the result in a repot.
				LoggerPlog().ReportInDirectory(total, qrFound, qrDecoded, inputArgument.at(dir_i));
			}
			// log the result in a repot.
			LoggerPlog().ReportInDirectory(gtotal, gQrFound, gQrDecoded, "GENERAL REPORT__");
		}
		
		//------------------------------- Work with camera -------------------------------------

		else if (sourceType == InputHelpers::LIVE_VIDEO) {
			VideoCapture vid(0);
			Mat edges;
			if (!vid.isOpened())
				throw exception("ERR: Unable to open the camera.\n");
			for (;;){
				vid >> inImg; // get a new frame from camera
				qrPos.clear();
				orderIndex.clear();
				// --------------------------------------------------------------------------------------
				// --------------------- Executing STAGE 2,3,4,5,6 ---------------------------------------
				// --------------------------------------------------------------------------------------
				stage = DecodeProcess(inImg, inImgBin, qrImg, qrPos, orderIndex, qrAlign, qrDecodedMessage);
				DisplaySteps(inImg, inImgBin, qrImg, qrPos, orderIndex, qrAlign, qrDecodedMessage, stage, DISP_ALL_CONTINUOUS);
			}
		}
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
		return -1;
	}
	return 0;
}