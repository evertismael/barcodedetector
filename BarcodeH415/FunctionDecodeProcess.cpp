/** Author: Evert Ismael Pocoma Copa
* Date : 09 - 04 - 2018
* Implements the stages 2,3,4,5 of the algorithm
*	stage 2: 'BINARIZATION - LOCAL THRESHOLDING'
*	stage 3: 'FIND QR-FINDERS'
*	stage 4: 'FIND QR-ALIGNMENT'
*	stage 5: 'EXTRACT QR-CODE'
*	stage 6: 'DECODE QR-CODE' 
**/

#include "stdafx.h"
using namespace std;
using namespace cv;

int DecodeProcess(Mat inImg, Mat &inImgBin, Mat &qrImg, vector<Point2i> &qrPos, vector<int> &orderIndex,Point2i &qrAlign, string &qrDecodedMessage){

	vector<vector<int>> qrSizes;	// qr-Finder dimension
	qrImg = cv::Mat::zeros(qrImg.size(), qrImg.type());		// reset the qrImage
	int stage = 1;
	qrDecodedMessage = "";
	
	// --------------------------------------------------------------------------------------
	// ---------------- STAGE 2: BINARIZATION - LOCAL THRESHOLDING --------------------------
	// --------------------------------------------------------------------------------------
	int blockSize = 0;
	if (inImg.rows < 512) {
		blockSize = 151;
	}
	else if (inImg.rows < 1024) {
		blockSize = 201;
	}
	else if (inImg.rows < 1500) {
		blockSize = 301;
	}
	else {
		blockSize = 351;
	}
	bruface::binarizeImg(inImg, inImgBin, 126, blockSize);
	stage = 2;
	
	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 3: FIND QR-FINDERS --------------------------------------
	// ------------------ and order them if they are 3 --------------------------------------
	bruface::getQRMarksPoints(inImgBin, qrPos, qrSizes);

	if (qrPos.size() < 3) {			// return if finders found are not enough
		return stage;
	}
	bruface::orderQrPoints(qrPos, orderIndex);	// order marks [qr1_ul, qr1_ll, qr1_ur];
	stage = 3;

	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 4: FIND QR-ALIGNMENT ------------------------------------
	// --------------------------------------------------------------------------------------

	bool isV1 = bruface::getAlignPoints(inImgBin, qrPos, qrSizes, orderIndex, qrAlign);
	stage = 4;

	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 5: EXTRACT QR-CODE --------------------------------------
	// --------------------------------------------------------------------------------------
	bruface::getQrImageFromFindersAndAlign(inImgBin, qrImg, qrPos, qrSizes, orderIndex, qrAlign, isV1);
	stage = 5;

	// --------------------------------------------------------------------------------------
	// ---------------------- STAGE 6: DECODE QR-CODE --------------------------------------
	// --------------------------------------------------------------------------------------
	if (QRdecode(qrImg, qrDecodedMessage) == 0) {
		stage = 6;
	}
	
	return stage;		
}