#pragma once
#include <ctime>
#include <plog/Log.h>
class LoggerPlog
{
public:
	static void LoggerInit();
	static void LogError(string msg);
	static void ReportInDirectory(int total, int qrFound, int qrDecoded, string directory);
};

