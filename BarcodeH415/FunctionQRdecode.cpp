#include "stdafx.h"
using namespace std;
using namespace zxing;
using namespace zxing::qrcode;
using namespace cv;
/*Functio for decoding the Qr-Code. At this point the qr code has to be already extracted.*/
int QRdecode(Mat qrImg, string &payload)
{
	bool multi = false; // flag multiformato. Change if another tipe of code.
	try
	{
		// Create luminance  source
		Ref<LuminanceSource> source = MatSource::create(qrImg);
		// Search for QR code
		Ref<Reader> reader;
		if (multi) {
			reader.reset(new MultiFormatReader);
		}
		else {
			reader.reset(new QRCodeReader);
		}

		Ref<Binarizer> binarizer(new GlobalHistogramBinarizer(source));
		Ref<BinaryBitmap> bitmap(new BinaryBitmap(binarizer));
		Ref<Result> result(reader->decode(bitmap, DecodeHints(DecodeHints::TRYHARDER_HINT)));
		payload = result->getText()->getText();
	}
	catch (const std::exception& ie)
	{
		cerr << ie.what() << endl;
		return -1;
	}
	return 0;
}
