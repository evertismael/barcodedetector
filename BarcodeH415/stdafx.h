// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
#include <iostream>

#include "BarcodeDetector.h"
#include "InputHelpers.h"
#include <opencv2/opencv.hpp>
#include <list>


#include <zxing/common/Counted.h>
#include <zxing/Binarizer.h>
#include <zxing/MultiFormatReader.h>
#include <zxing/Result.h>
#include <zxing/ReaderException.h>
#include <zxing/common/GlobalHistogramBinarizer.h>
#include <zxing/Exception.h>
#include <zxing/common/IllegalArgumentException.h>
#include <zxing/BinaryBitmap.h>
#include <zxing/DecodeHints.h>
#include <zxing/qrcode/QRCodeReader.h>
#include <zxing/MultiFormatReader.h>
#include <zxing/MatSource.h>


#include "LoggerPlog.h"

#ifndef STDAFX
#define STDAFX
int QRdecode(Mat qrImg, string &qrDecodedMessage);
int DecodeProcess(Mat inImg, Mat &inImgBin, Mat &qrImg, vector<Point2i> &qrPos, vector<int> &orderIndex, Point2i &qrAlign, string &qrDecodedMessage);
void DisplaySteps(Mat inImg, Mat inImgBin, Mat qrImg, vector<Point2i> qrPos, vector<int> orderIndex, Point2i qrAlign, string qrDecodedMessage,int stage, int mode);
#endif 

