anotation.csv is structured as follows:

filename;quoteID;QRcodesize;light;%cover;rotation;perspective;blur

QRcodesize	{L,M,Q,H}
light		{0,1}  (0 if lighting is constant)
%cover 		{0-30, 30-70, 70-100}
rotation	{0,1}
perspective 	{0,1}
blur		{0,1}
