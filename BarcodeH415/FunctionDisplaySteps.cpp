#include "stdafx.h"
using namespace std;
using namespace cv;

#define DISP_ALL_CONTINUOUS 3
#define DISP_ALL_PAUSED 2
#define DISP_CONSOLE 1

#define UL 0
#define LL 1
#define UR 2

void DisplaySteps(Mat inImg, Mat inImgBin, Mat qrImg, vector<Point2i> qrPos, vector<int> orderIndex, Point2i qrAlign, string qrDecodedMessage, int stage, int mode){
	Mat stage3, stage4;
	// ------------- draw the output --------------------
	if (mode == DISP_ALL_PAUSED || mode == DISP_ALL_CONTINUOUS) {
		
		namedWindow("input", WINDOW_GUI_NORMAL);
		cv::moveWindow("input", 0, 0);
		cv::resizeWindow("input", 700, 420);
				
		namedWindow("stage2", WINDOW_GUI_NORMAL);
		moveWindow("stage2", 705, 0);
		resizeWindow("stage2", 700, 420);

		namedWindow("stage3", WINDOW_GUI_NORMAL);
		moveWindow("stage3", 0, 455);
		resizeWindow("stage3", 700, 420);

		namedWindow("stage4", WINDOW_GUI_NORMAL);
		moveWindow("stage4", 705, 455);
		resizeWindow("stage4", 700, 420);

		namedWindow("QR-stage5", WINDOW_GUI_NORMAL);
		cv::moveWindow("QR-stage5", 641, 0);
		cv::resizeWindow("QR-stage5", 200, 200);

		if (stage < 2) {
			inImgBin = Mat::zeros(350, 350, CV_8UC1);
		}

		if (stage >= 3) {
			inImgBin.copyTo(stage3);
			circle(stage3, qrPos.at(orderIndex.at(UL)), 10, (0, 0, 200), -1);
			circle(stage3, qrPos.at(orderIndex.at(LL)), 10, (0, 0, 200), -1);
			circle(stage3, qrPos.at(orderIndex.at(UR)), 10, (0, 0, 200), -1);
		}
		else {
			stage3 = Mat::zeros(350, 350, CV_8UC1);
		}
		
		if (stage >= 4) {
			inImgBin.copyTo(stage4);
			circle(stage4, qrAlign, 5, (0, 0, 200), -1);
			putText(stage4, "Align", qrAlign, FONT_HERSHEY_SIMPLEX, 2, (0, 0, 200), 5, LINE_AA);
			putText(stage4, "UL", qrPos.at(orderIndex.at(UL)), FONT_HERSHEY_SIMPLEX, 2, (0, 0, 200), 5, LINE_AA);
			putText(stage4, "LL", qrPos.at(orderIndex.at(LL)), FONT_HERSHEY_SIMPLEX, 2, (0, 0, 200), 5, LINE_AA);
			putText(stage4, "UR", qrPos.at(orderIndex.at(UR)), FONT_HERSHEY_SIMPLEX, 2, (0, 0, 200), 5, LINE_AA);	
		}
		else {
			stage4 = Mat::zeros(350, 350, CV_8UC1);
		}
		if (stage < 5) {
			qrImg = Mat::zeros(350, 350, CV_8UC1);
		}


		if (stage >= 1) {
			imshow("input", inImg);
			imshow("stage2", inImgBin);
			imshow("stage3", stage3);
			imshow("stage4", stage4);
			imshow("QR-stage5", qrImg);
			waitKey(32);
		}
	}

	if (mode == DISP_ALL_PAUSED) {
		waitKey();
	}
	if (qrDecodedMessage != "") {
		cout << "qrMessage: " << qrDecodedMessage << endl;
	}
}