#pragma once
#include <opencv2/opencv.hpp>

class InputHelpers{
public:
	enum SourceTypes { NONE = 0, SINGLE_IMAGE, DIRECTORY, LIVE_VIDEO };
	static void ParseInputParams(int argc, char* argv[], InputHelpers::SourceTypes& sourceType, std::vector<std::string> &inputArgument);
	static void GetImages(string directory, vector<cv::String> &images);
};

