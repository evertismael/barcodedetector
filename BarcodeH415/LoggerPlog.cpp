#include "stdafx.h"
#include "LoggerPlog.h"


namespace plog
{
	class MyFormatter
	{
	public:
		static util::nstring header() // This method returns a header for a new file. In our case it is empty.
		{
			return util::nstring();
		}

		static util::nstring format(const Record& record) // This method returns a string from a record.
		{
			util::nostringstream ss;
			ss <<"["<<record.getSeverity() <<":"<<record.getLine()<<"]"<< record.getMessage() << "\n"; // Produce a simple string with a log message.

			return ss.str();
		}
	};
}


void LoggerPlog::LoggerInit()
{
	time_t now = time(0);
	tm *ltm = localtime(&now);
	// formating the name of loggerFile.
	std::string year = std::to_string(1900 + ltm->tm_year);
	std::string month = std::to_string(1 + ltm->tm_mon);
	std::string day = std::to_string(ltm->tm_mday);
	std::string hour = std::to_string(ltm->tm_hour);
	std::string min = std::to_string(1 + ltm->tm_min);
	std::string sec = std::to_string(1 + ltm->tm_sec);
	std::string fileName = day + "_"+ month +"_"+ year + "_" + hour+ "_" + min+"_" + sec + ".txt";
	const char *name = fileName.c_str();

	plog::init<plog::MyFormatter>(plog::debug, name); // Initialize the logger and pass our formatter as a template parameter to init function.

}


void LoggerPlog::LogError(string msg)
{
	LOGD << msg;
}

void LoggerPlog::ReportInDirectory(int total,  int qrFound, int qrDecoded, string directory)
{
	if (total > 0) {
		LOGD << "------------------------------------------";
		LOGD << "DIRECTORY: " + directory;
		LOGD << "------ total: " << total;
		LOGD << "------ qrFound: " << qrFound;
		LOGD << "------ qrFound %: " << qrFound*100/total;
		LOGD << "------ qrDecoded: " << qrDecoded;
		if (qrFound>0) 
			LOGD << "------ qrDecoded %: " << qrDecoded*100/ qrFound;
	}
}